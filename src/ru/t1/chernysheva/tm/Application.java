package ru.t1.chernysheva.tm;

import ru.t1.chernysheva.tm.constant.ArgumentsConst;

public class Application {

    public static void main(String[] args) {
        processArguments(args);
    }

    public static void processArguments(final String[] arguments) {
        if (arguments == null || arguments.length < 1 ) {
            showError();
            return;
        }
        processArgument(arguments[0]);
    }

    public static  void showError () {
        System.out.println("[ERROR]");
        System.err.println("The program argument is not correct.");
    }

    public static void processArgument(final String argument) {
        switch (argument) {
            case ArgumentsConst.VERSION:
                showVersion();
                break;
            case ArgumentsConst.HELP:
                showHelp();
                break;
            case ArgumentsConst.ABOUT:
                showAbout();
                break;
            default:
                showError();
        }
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.2.0");
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Sofia Chernysheva");
        System.out.println("E-mail: schernysheva@t1-consulting.ru");
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s - Show version info.\n", ArgumentsConst.VERSION);
        System.out.printf("%s - Show developer info.\n", ArgumentsConst.ABOUT);
        System.out.printf("%s - Show command list.\n", ArgumentsConst.HELP);
    }

}
